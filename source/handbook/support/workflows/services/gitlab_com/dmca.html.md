---
layout: markdown_page
title: DMCA Removal Requests
category: GitLab.com
---

### On this page
{:.no_toc}

- TOC
{:toc}

----

As of March 2019, the Abuse Team has taken ownership of [DMCA requests](https://about.gitlab.com/handbook/engineering/security/dmca-removal-requests.html).